const people = [
    { name: 'Anna', age: 15 },
    { name: 'Ivan', age: 21 },
    { name: 'Maria', age: 18 },
    { name: 'Vlad', age: 35 },
    { name: 'Bob', age: 5 }

];

//ascending sorting 
people.sort(mySort);
function mySort(a, b) {
    if (a.age > b.age)
        return 1
    if (a.age < b.age)
        return -1;
    return 0;
}
console.log(people);

//descending sorting
people.sort(mySortDescending);
function mySortDescending(a, b) {
    if (a.age > b.age)
        return -1
    if (a.age < b.age)
        return 1;
    return 0;
}

console.log(people)

//another ways of sorting

//ascending sorting 
console.log(people.sort(x => x.age));

//descending sorting 
console.log(people.sort(x => x.age).reverse());
