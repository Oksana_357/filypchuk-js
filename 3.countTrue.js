function countTrue(arr) {
    return arr.filter(x => x == true).length;
}
console.log(countTrue([true, false, false, true, false]));

console.log(countTrue([false, false, false, false]));

console.log(countTrue([]));
