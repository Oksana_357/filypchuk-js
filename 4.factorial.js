function doublefactorial(n) {
    if (n == 0 || n == 1)
        return 1;
    return n * doublefactorial(n - 2);
}

console.log(doublefactorial(0));

console.log(doublefactorial(2));

console.log(doublefactorial(9));

console.log(doublefactorial(14));