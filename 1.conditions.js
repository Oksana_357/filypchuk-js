
//If x < y: sum
//If x > y: difference
//Otherwise: product


let result = function (x, y) {

    if (x < y)
        return x + y;
    else if (x > y)
        return x - y;
    else
        return x * y;
}

console.log(result(10, 20));

console.log(result(20, 10));

console.log(result(10, 10));

//Or different variant of code

let resultValue;
function resultFunction(x, y) {
    if (x < y)
        resultValue = x + y;
    else if (x > y)
        resultValue = x - y;
    else
        resultValue = x * y;
}
resultFunction(10, 20)
console.log(resultValue);

resultFunction(20, 10)
console.log(resultValue);

resultFunction(10, 10)
console.log(resultValue);

