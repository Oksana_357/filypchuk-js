const arrayA = [1, 2, 3, -5, 0, 10];
const arrayB = [5, -1, 7];

let sumA = arrayA.reduce((prev, item) => {
    return prev + item;
});

let sumB = arrayB.reduce((prev, item) => {
    return prev + item;
});


console.log(sumA + sumB);

// Second way to sum arrays


function reduseTwoArrays(arrayA, arrayB) {
    let sumA = arrayA.reduce((prev, item) => {
        return prev + item;
    });

    let sumB = arrayB.reduce((prev, item) => {
        return prev + item;
    });
    return sumA + sumB;
}

console.log(reduseTwoArrays(arrayA, arrayB));


